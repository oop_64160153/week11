package com.ubolthip.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();

        Plane boeing = new Plane("Boeing", "Rolls-Royce");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();

        Superman clark = new Superman("Clark");
        clark.eat();
        clark.sleep();
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.swim();

        Human man1 = new Human("Man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();

        Bat bat1 = new Bat("Megabat");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Rat rat1 = new Rat("Jerry");
        rat1.eat();
        rat1.sleep();
        rat1.walk();
        rat1.run();

        Dog golden = new Dog("Golden");
        golden.eat();
        golden.sleep();
        golden.walk();
        golden.run();
        golden.swim();

        Cat bengal = new Cat("Bengal");
        bengal.eat();
        bengal.sleep();
        bengal.walk();
        bengal.run();
        bengal.swim();

        Crocodile crocodile1 = new Crocodile("Croco");
        crocodile1.eat();
        crocodile1.sleep();
        crocodile1.swim();
        crocodile1.crawl();

        Fish anemonefish = new Fish("Nemo");
        anemonefish.eat();
        anemonefish.sleep();
        anemonefish.swim();

        Snake snake1 = new Snake("Python");
        snake1.eat();
        snake1.sleep();
        snake1.crawl();

        System.out.println();
        System.out.println("--Flyables--");
        Flyable[] flyables = { bird1, boeing, clark, bat1 };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        System.out.println();
        System.out.println("--Walkables--");
        Walkable[] walkables = { bird1, clark, man1, rat1, golden, bengal };
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        System.out.println();
        System.out.println("--Swimables--");
        Swimable[] swimables = { clark, man1, golden, bengal, crocodile1, anemonefish };
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        System.out.println();
        System.out.println("--Crawlable--");
        Crawlable[] crawlables = {crocodile1, snake1 };
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();
        }
    }
}
